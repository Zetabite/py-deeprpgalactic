import pickle
from os import listdir
from os.path import isfile, join
import os
def uncompress(path):
    with open (str(path), 'rb') as fp:
        return pickle.load(fp)
def compress(path,data):
    with open(str(path), 'wb') as fp:
        pickle.dump(data, fp)

def RepresentsInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

cls = lambda: os.system('cls' if os.name =='nt' else 'clear')

filesInDir = lambda path, type: [f for f in listdir(path) if isfile(join(path, f)) and f.lower().endswith(type)]

beginsWithString = lambda string,dest: string in dest[0:len(string)]

matches = lambda string,array: [s for s in array if beginsWithString(string,s)]

ascii_numbers_dez = [48 + i for i in range(10)]
ascii_upper_dez = [65 + i for i in range(26)]
ascii_lower_dez = [97 + i for i in range(26)]
ascii_letters_dez = ascii_upper_dez + ascii_lower_dez
ascii_numbers = [chr(n) for n in ascii_numbers_dez]
ascii_upper = [chr(c) for c in ascii_upper_dez]
ascii_lower = [chr(c) for c in ascii_lower_dez]
ascii_letters = [chr(c) for c in ascii_letters_dez]

def getMatcher(string,array):
    for s in array:
        if beginsWithString(string,s):
            return s