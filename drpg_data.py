import json
import os
import drpg_util as util

#directories
def dirpath():
    return str(os.path.dirname(os.path.abspath(__file__)))
def savepath():
    return str(dirpath() + "/save/")
def datapath():
    return str(dirpath() + "/data/")
def mappath():
    return str(savepath() + "map/")
def readme():
    readme = open(dirpath() + "/readme.txt",'r')
    input(readme.read() + '\n')
    readme.close()

#json files
def regionsJSON():
    return json.load(open(datapath() + "regions.json"))
def propertiesJSON():
    return json.load(open(datapath() + "properties.json"))
def materialsJSON():
    return json.load(open(datapath() + "materials.json"))
def dwarfsJSON():
    return json.load(open(datapath() + "characters.json")) 

getJSON = lambda path: json.load(path)

def createJSON(path,data):
    output = open(path,'w')
    output.write(json.dumps(data, indent = 4, sort_keys=False))
    output.close()

def save(save,cave):
    if not os.path.exists(mappath()):
        os.makedirs(mappath())
    util.compress(mappath() + save["cave"], cave)
    createJSON(savepath() + save["name"] + ".json", save)