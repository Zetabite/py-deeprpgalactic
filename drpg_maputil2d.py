import json
import random
import drpg_data as data
import drpg_noise as noiselib

def genCave(mission):
    region = mission["region"]
    rockid = int(region.get("rock"))
    properties = data.propertiesJSON().get("cave-gen")
    maxWidth = int(properties.get("maxWidth"))
    maxDepth = int(properties.get("maxDepth"))
    width = int(random.randrange(int(maxWidth/2),maxWidth,25))
    depth = int(random.randrange(int(maxDepth/2),maxDepth,25))
    #rockid = 10
    #width = 6#maxWidth
    #depth = 5#maxDepth
    cave = getCaveBase(width,depth,rockid)

    variation = 2
    seed = noiselib.genSeed()
    noise = noiselib.createNoise(variation,seed)
    noisesize = len(noise)
    blocklist = sorted(noiselib.getUniqueValues(noise, variation))
    overlay = [[0 for _ in range(width)] for _ in range(depth)]
    i = 0
    for z in range(depth):
        for x in range(width):
            index = i % noisesize
            value = noise[index]
            block = 0
            if value in blocklist[0:1]:
                if int(value) != 0:
                    block = rockid
            overlay[z][x] = block
            i += 1
    print(cave)
    print(overlay)
    for z in range(depth):
        for x in range(width):
            if cave[z][x] != rockid and overlay[z][x] != 0:
                cave[z][x] = overlay[z][x]
    print(cave)
    return [cave,overlay]

def getCaveBase(width,depth,rockid):
    cave = [[0 for _ in range(width)] for _ in range(depth)]
    for z in range(depth):
        for x in range(width):
            block = 0
            if x in [0, width - 1] or z in [0, depth - 1]:
                block = rockid
            cave[z][x] = block
    return cave

def getBlockName(id):
    material = data.materialsJSON().get(str(id))
    materialName = material.get("name")
    return materialName

def makeReadable(cave):
    width = len(cave)
    depth = len(cave[0])
    translated = [["" for _ in range(width)] for _ in range(depth)]
    for z in range(depth):
        for x in range(width):
            translated[z][x] = getBlockName(cave[z][x])
    return translated