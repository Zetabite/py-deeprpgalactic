def loadMap(type):
    files = [f for f in listdir(mappath) if isfile(join(mappath, f)) and f.lower().endswith(type)]
    cls()
    if len(files) > 0:
        page = [[]]
        pageCount = 0
        counter = 0
        for file in files:
            if counter < 5:
                page[pageCount].append(file)
                counter += 1
            else:
                pageCount += 1
                counter = 0
        counter = 0
        for file in page[0]:
            print("[%d] %s" % (counter, file))
            counter += 1
        selection = str(input("Select file: "))
        if selection == "":
            toolMenu()
        else:
            selection = int(selection)
        selection = page[0][selection]
        with open (mappath + selection, 'rb') as fp:
            cave = pickle.load(fp)
    else:
        print("No map existing.")
        toolMenu()
