import random
from drpg_util import ascii_numbers, ascii_letters

map = ascii_numbers + ascii_letters

genSeed = lambda size=128: ''.join([random.choice(map) for _ in range(size)])
noseed = "d33pr0ckg4l4ct1c"

def createNoise(variation, seed=noseed,size=512):
    noise = []
    # apply variation to seed
    seed = str(seed)
    missing = variation - len(seed) % variation
    if missing > 0:
        addition = ""
        for i in range(missing):
            index = i % len(seed)
            addition += seed[index]
        seed += addition
    chunksize = int(len(seed) / variation)

    #permutation
    prepermutation = []
    permutation = []
    for c in seed:
        if c in map:
            prepermutation.append(ord(c))
    for i in range(variation):
        chunk = prepermutation[i:i+chunksize]
        permutation.append(sum(chunk))
    
    #creating the noise
    for i in range(size):
        preindex = i % len(prepermutation)
        index = prepermutation[preindex] % len(permutation)
        noise.append(permutation[index])
    return noise

def normalize(noise, variation):
    uniques = getUniqueValues(noise, variation)
    normalized = []
    step = 1 / variation
    for e in noise:
        for i in range(variation):
            if uniques[i] == e:
                normalized.append(step * i)
    return normalized

def getUniqueValues(noise, variation):
    uniques = []
    for e in noise:
        if e not in uniques:
            uniques.append(e)
        if len(uniques) == variation:
            return uniques
            
def createNormalizedNoise(variation,seed=noseed,size=512):
    noise = createNoise(variation,seed,size)
    normalized = normalize(noise,variation)
    return normalized