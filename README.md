# PY-DeepRPGalactic
This project is a 'roguelike textadventure' interpretation of the PC game 'Deep Rock Galactic'.
*(Happy Birthday DRG btw.)*
My project is unofficial, but bases parts of its lore and mechanics on the original game,
but will eventually feature its own ideas (which is already have, kinda).
If you plan to play the game and don't want to be spoiled, you may not want to check out the data folder.
Is it overengineered on some parts. Probably. Could there have been done anything better?
Definetly. Do I care? Yes. Hence I upload this here, so you can contribute if you want to. :D

#What you need to play:
* Yourself
* Python
* No knowledge of the original game
* No additional dependencies

#So, what feature do exist currently:
* No Gameplay, mostly just engine and data stuff
* Generation of basic caves (with coherent noise!)
* Easy expandable material, region and mission repertoire
* Menu
* Save/Load feature

Have Fun
**Rock and Stone!**
