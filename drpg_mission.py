import drpg_data as data
import random

def genObjective(mission_type):
    mission_objective = data.propertiesJSON().get("mission").get(mission_type)
    primary = genPrimaryObjective(mission_objective)
    secondary = genSecondaryObjective(mission_objective)
    return [primary,secondary]

def genPrimaryObjective(mission_objective):
    objective = str(random.choice(list(mission_objective.get("primary"))))
    amount = random.randrange(150,400,5)
    if objective == "4":
        amount = random.randrange(2,10,2)
    return [objective, amount]

def genSecondaryObjective(mission_objective):
    objective = str(random.choice(list(mission_objective.get("secondary"))))
    amount = random.randrange(10,25,5)
    if objective == "7":
        amount = random.randrange(50,200,50)
    return [objective, amount]

def genMission(planetName="hoxxes4", mission_type="mining"):
    planet = data.regionsJSON().get(planetName)
    region = random.choice(list(planet.get("regions")))

    mission_objective = data.propertiesJSON().get("mission").get(mission_type)

    primary = genPrimaryObjective(mission_objective)
    secondary = genSecondaryObjective(mission_objective)

    mission = {}
    mission["type"] = mission_type
    mission["planet"] = planet
    mission["region"] = planet.get("regions").get(region)
    mission["primary"] = primary[0]
    mission["primaryAmnt"] = primary[1]
    mission["secondary"] = secondary[0]
    mission["secondaryAmnt"] = secondary[1]
    return mission