import json
import random

def genCave(properties,region):
    maxWidth = properties.get("maxWidth")
    maxDepth = properties.get("maxDepth")
    maxHeight = properties.get("maxHeight")
    #width = int(random.randrange(maxWidth/2,maxWidth,25))
    #depth = int(random.randrange(maxDepth/2,maxDepth,25))
    #height = int(random.randrange(maxHeight/2,maxHeight,25))
    width = maxWidth
    depth = maxDepth
    height = maxHeight

    cave = [[[0]*width]*height]*depth
    for x in range(width):
        yaxis = {}
        for y in range(height):
            zaxis = {}
            for z in range(depth):
                cave[x][y][z] = getBaseBlock(x,y,z,width,height,depth)
    return cave

def getBaseBlock(x,y,z,width,height,depth):
    block = 0
    #empty = 0
    if x in [0,width - 1] or y in [0,height - 1] or z in [0,depth - 1]:
        #plot-fabric = -1
        block = 1
    return block