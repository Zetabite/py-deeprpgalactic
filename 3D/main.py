import json
import random
from os import listdir
from os.path import isfile, join
import os
from maputil import genCave
import pickle

dirpath = os.path.dirname(os.path.abspath(__file__))
savepath = dirpath + "/save/"
datapath = dirpath + "/data/"
mappath = savepath + "/map/"

# loading json files
regions = json.load(open(datapath + "regions.json"))
properties = json.load(open(datapath + "properties.json"))
materials = json.load(open(datapath + "materials.json"))
dwarfs = json.load(open(datapath + "characters.json"))

def cls():
    os.system('cls' if os.name =='nt' else 'clear')

def genMission():
    mission_type = "mining"
    mission_objective = properties.get("mission").get(mission_type)
    planet = regions.get("hoxxes4")
    region = planet.get("regions").get(random.choice(list(regions.get("hoxxes4").get("regions"))))
    primary = materials.get(random.choice(list(mission_objective.get("primary")))).get("name")
    secondary = materials.get(random.choice(list(mission_objective.get("secondary")))).get("name")
    primaryAmount = random.randrange(150,400,5)
    if primary == "alien-egg":
        primaryAmount = random.randrange(4,10,2)
    secondaryAmount = random.randrange(10,25,5)
    print("Mission:")
    print("Region: %s on %s" % (region.get("name"), planet.get("name")))
    print("Primary: %s %d" % (primary, primaryAmount))
    print("Secondary: %s %d" % (secondary, secondaryAmount))
    mission = {}
    mission["type"] = mission_type
    mission["region"] = region
    mission["primary"] = primary
    mission["primaryAmount"] = primaryAmount
    mission["secondary"] = secondary
    mission["secondaryAmount"] = secondaryAmount
    return mission

def getCharacter():
    characters = list(dwarfs.keys())
    print("Choose your dwarf:")
    for c in characters:
        if c not in characters[4:]:
            name = dwarfs.get(c).get("name")
            print("- [%s]%s - %s" % (name[0],name[1:],dwarfs.get(c).get("desc")))
    selection = str(input("Your pick: ")).lower()
    if len(selection) == 1:
        for c in characters:
            if selection == dwarfs.get(c).get("name").lower()[0]:
                selection = c
    cls()
    if selection in characters:
        print("%s - %s:\n" % (dwarfs.get(selection).get("name"),dwarfs.get(selection).get("desc")))
        print("%s\n" % dwarfs.get(selection).get("long-desc"))
        value = input("confirm choice[y/N]: ").lower()
        if value == "y":
            return selection
        elif value in ["exit","quit"]:
            exit()
    elif selection in ["back","return"]:
        mainMenu()
    elif selection in ["exit","quit"]:
        exit()
    cls()
    return getCharacter()

def leaveGame(character,mission,cave,action):
    value = str(input("Do you want to save[y/N]: ")).lower()
    if value in ['y','n']:
        if value == 'y':
            saveGame(character,mission,cave)
        if action == "exit":
            exit()
        elif action == "menu":
            mainMenu()
    else:
        leaveGame(character,mission,cave,action)

def playGame(save):
    cls()
    character = save[0]
    mission = save[1]
    cave = save[2]
    print(character)
    print(mission)
    print(cave)
    value = input("Action: ")
    if value == "save":
        saveGame(character,mission,cave)
    elif value in ["quit","exit","menu"]:
        leaveGame(character,mission,cave,value)

def newGame():
    character = getCharacter()
    mission = genMission()
    cave = genCave(properties.get("cave-gen"), mission.get)
    return [character,mission,cave]

def saveGame(character,mission,cave):
    cls()
    saveName = str(input("Input save file name: "))
    save = {}
    save["name"] = saveName
    save["character"] = character
    save["mission"] = mission
    save["cave"] = saveName + ".map"
    with open(mappath + save["cave"], 'wb') as fp:
        pickle.dump(cave, fp)
    output = open(savepath + saveName + ".json",'w')
    output.write(json.dumps(save, indent = 4, sort_keys=False))
    output.close()

def loadGame():
    files = [f for f in listdir(savepath) if isfile(join(savepath, f)) and f.lower().endswith(".json")]
    cls()
    if len(files) > 0:
        page = [[]]
        pageCount = 0
        counter = 0
        for file in files:
            if counter < 5:
                page[pageCount].append(file)
                counter += 1
            else:
                pageCount += 1
                counter = 0
        counter = 0
        for file in page[0]:
            name = json.load(open(savepath + file)).get("name")
            print("[%d] %s" % (counter, name))
            counter += 1
        selection = str(input("Select save: "))
        if selection == "":
            mainMenu()
        else:
            selection = int(selection)
        print(page[0][selection])
        selection = json.load(open(savepath + file))
        character = selection.get("character")
        mission = selection.get("mission")
        with open (mappath + selection.get("cave"), 'rb') as fp:
            cave = pickle.load(fp)
        return [character,mission,cave]
    else:
        print("No save existing.")
        mainMenu()

def loadMap(type):
    files = [f for f in listdir(mappath) if isfile(join(mappath, f)) and f.lower().endswith(type)]
    cls()
    if len(files) > 0:
        page = [[]]
        pageCount = 0
        counter = 0
        for file in files:
            if counter < 5:
                page[pageCount].append(file)
                counter += 1
            else:
                pageCount += 1
                counter = 0
        counter = 0
        for file in page[0]:
            print("[%d] %s" % (counter, file))
            counter += 1
        selection = str(input("Select file: "))
        if selection == "":
            toolMenu()
        else:
            selection = int(selection)
        selection = page[0][selection]
        with open (mappath + selection, 'rb') as fp:
            cave = pickle.load(fp)
    else:
        print("No map existing.")
        toolMenu()

def mainMenu():
    rows, columns = os.popen('stty size', 'r').read().split()

    if int(columns) > 130:
        banner = open(datapath + "banner.txt",'r')
        print(banner.read() + '\n')
        banner.close()
    print("Menu:\n")
    print("[N]ew Game")
    print("[L]oad Game")
    print("[R]eadme")
    print("[Q]uit")
    print("[T]ools")
    value = input().lower()
    cls()
    if value in ["n","new"]:
        playGame(newGame())
    elif value in ["l","load"]:
        playGame(loadGame())
    elif value in ["r","readme"]:
        readme = open(dirpath + "/readme.txt",'r')
        input(readme.read() + '\n')
        readme.close()
        mainMenu()
    elif value in ["q","exit","quit"]:
        exit()
    elif value in ["t","tools"]:
        toolMenu()
    else:
        mainMenu()


def toolMenu():
    print("Tools:")
    print("[M]ap to Json")
    print("[J]son to Map")
    print("[B]ack")
    value = input().lower()
    cls()
    if value in ["m","map"]:
        loadMap(".map")
    elif value in ["j","json"]:
        loadMap(".json")
    elif value in ["b","back"]:
        mainMenu()
    else:
        toolMenu()

def main():
    cls()
    mainMenu()

if __name__ == '__main__':
    main()