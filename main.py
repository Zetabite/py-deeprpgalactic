import json
import os
from drpg_maputil2d import genCave, makeReadable
from drpg_tools import loadMap
import drpg_data as data
import drpg_util as util
from drpg_util import cls,matches,getMatcher,beginsWithString
from drpg_mission import genMission

#cave_data = [cave,changes] -> seed?

dirpath = data.dirpath()
savepath = data.savepath()
datapath = data.datapath()
mappath = data.mappath()

# loading json files
regions = data.regionsJSON()
properties = data.propertiesJSON()
materials = data.materialsJSON()
dwarfs = data.dwarfsJSON()

def getCharacter():
    characters = list(dwarfs.keys())
    print("Choose your dwarf:")
    for c in characters:
        if c not in characters[4:]:
            name = dwarfs.get(c).get("name")
            print("- [%s]%s - %s" % (name[0],name[1:],dwarfs.get(c).get("desc")))
    selection = str(input("Your pick: ")).lower()
    cls()
    if matches(selection,characters):
        selection = getMatcher(selection,characters)
        print("%s - %s:\n" % (dwarfs.get(selection).get("name"),dwarfs.get(selection).get("desc")))
        print("%s\n" % dwarfs.get(selection).get("long-desc"))
        value = input("confirm choice[y/N]: ").lower()
        if beginsWithString(value,"yes"):
            return selection
        elif matches(value,["exit","quit"]):
            exit()
    elif matches(selection,["back","return"]):
        main()
    elif matches(selection,["exit","quit"]):
        exit()
    cls()
    return getCharacter()

def leaveGame(character,mission,cave_data,action):
    value = str(input("Do you want to save[Y/n]: ")).lower()
    if matches(value,["yes","no"]):
        if beginsWithString(value,"yes"):
            saveGame(character,mission,cave_data)
        if beginsWithString(action,"exit"):
            exit()
        elif beginsWithString(action,"menu"):
            main()
    else:
        leaveGame(character,mission,cave_data,action)

def playGame(save):
    #cls()
    character = save[0]
    mission = save[1]
    cave_data = save[2]
    #print(character)
    #for entry in makeReadable(cave_data[0]):
    #    print(entry)
    value = input("Action: ")
    if matches(value,["save"]):
        saveGame(character,mission,cave_data)
    elif matches(value,["quit","exit","menu"]):
        leaveGame(character,mission,cave_data,value)

def newGame():
    character = getCharacter()
    mission = genMission()
    #print("Mission:")
    #print("Region: %s on %s" % (mission["region"], mission["planet"]))
    #print("Primary: %s %d" % (mission["primary"], mission["primaryAmnt"]))
    #print("Secondary: %s %d" % (mission["secondary"], mission["secondaryAmnt"]))
    cave_data = genCave(mission)
    return [character,mission,cave_data]

def saveGame(character,mission,cave_data):
    cls()
    saveName = str(input("Input save file name: "))
    save = {}
    save["name"] = saveName
    save["character"] = character
    save["mission"] = mission
    save["cave_data"] = saveName + ".map"
    data.save(save,cave_data)

def loadGame(files=[],alreadyRan=False):
    cls()
    if len(files) > 0:
        #create page
        entryCnt = 0
        maxEntry = 5
        page = [""]*maxEntry
        for file in files:
            if entryCnt < maxEntry:
                name = data.getJSON(savepath + file).get("name")
                page[entry] = page[entryCnt] + ("[%d] %s" % (entryCnt, name))
                entryCnt += 1
            else:
                entryCnt = 0
        for entry in page:
            if entry != '':
                print(entry)
        #select
        selection = str(input("Select save: "))
        if selection == "":
            return loadGame(files)
        else:
            if util.RepresentsInt(selection):
                selection = json.load(open(savepath + files[int(selection)]))
                character = selection.get("character")
                mission = selection.get("mission")
                cave_data = util.uncompress(mappath + selection.get("cave_data"))
                return [character,mission,cave_data]
            else:
                return loadGame(files)
    else:
        if not alreadyRan:
            files = util.filesInDir(savepath,".json")
            return loadGame(files,True)
        else:
            input("No save existing.")
            main()

def main():
    cls()
    rows, columns = os.popen('stty size', 'r').read().split()

    if int(columns) > 130:
        banner = open(datapath + "banner.txt",'r')
        print(banner.read() + '\n')
        banner.close()
    print("Menu:\n")
    print("[N]ew Game")
    print("[L]oad Game")
    print("[R]eadme")
    print("[Q]uit")
#    print("[T]ools")
    value = input().lower()
    cls()
    if beginsWithString(value,"new"):
        playGame(newGame())
    elif beginsWithString(value,"load"):
        playGame(loadGame())
    elif beginsWithString(value,"readme"):
        data.readme()
    elif matches(value,["exit","quit"]):
        exit()
#    elif value in ["t","tools"]:
#        toolMenu()
    else:
        main()

def toolMenu():
    print("Tools:")
    print("[M]ap to Json")
    print("[J]son to Map")
    print("[B]ack")
    value = input().lower()
    cls()
    if value in ["m","map"]:
        loadMap(".map")
    elif value in ["j","json"]:
        loadMap(".json")
    elif value in ["b","back"]:
        main()
    else:
        toolMenu()

#def main():
#    cls()
#    mainMenu()

if __name__ == '__main__':
    main()